#!/usr/bin/env python3

import argparse
import random
import time
from enum import IntEnum, auto
from functools import partial
from pathlib import Path
from typing import TypeAlias

import constraint
import yaml


class Person(IntEnum):
    FRITZ = auto()
    JOSIE = auto()
    RYAN = auto()
    TREY = auto()
    ADAM = auto()
    BEN = auto()
    GERALDINE = auto()
    MIKE = auto()
    BROOKE = auto()
    PAULETTE = auto()
    WALT = auto()
    NANNY = auto()
    PAPA = auto()
    REDACTED = auto()


GiversReceivers: TypeAlias = dict[Person, Person]


BOEHMS = {Person.FRITZ, Person.JOSIE, Person.RYAN, Person.TREY}
HAZELS = {Person.ADAM, Person.BEN, Person.GERALDINE, Person.MIKE}
TATUMS = {Person.BROOKE, Person.PAULETTE, Person.WALT}
OG_BOEHMS = {Person.NANNY, Person.PAPA}

FAMILIES = [BOEHMS, HAZELS, TATUMS, OG_BOEHMS]
EVERYONE = set.union(*FAMILIES)


def family_for_person(person: Person) -> set[Person]:
    return next(family for family in FAMILIES if person in family)


class GiftExchange:
    def __init__(self) -> None:
        self.constraints = {person: EVERYONE - family_for_person(person) for person in EVERYONE}
        self.history = self.read_history()

    @classmethod
    def read_history(cls) -> dict[int, GiversReceivers]:
        """
        Read and parse the gift exchange history file.
        """
        history_file = Path(__file__).parent / "history.yaml"
        raw_history = yaml.safe_load(history_file.read_text())
        history = {}
        for year, mapping in raw_history.items():
            history[year] = {}
            for giver, receiver in mapping.items():
                history[year][Person.__members__[giver.upper()]] = Person.__members__[receiver.upper()]
        return history

    @classmethod
    def get_history(cls, year: int):
        history = cls.read_history()
        return history[year]

    def require_link(self, giver: Person, receiver: Person):
        """
        Require a ``giver`` -> ``receiver`` link in the solution.
        """
        assert receiver in self.constraints[giver]
        self.constraints[giver] = {receiver}

    def forbid_link(self, giver: Person, receiver: Person):
        """
        Forbid a ``giver`` -> ``receiver`` link in the solution.
        """
        if receiver in self.constraints.get(giver, set()):
            self.constraints[giver].remove(receiver)

    def forbid_history(self, year: int):
        """
        Forbid all links from a given year.
        """
        mapping = self.history[year]
        for giver, receiver in mapping.items():
            self.forbid_link(giver, receiver)

    def match_history(self, year: int):
        """
        Match [partial] history for a given year.

        Used to make minimal revisions to a mapping.
        """
        mapping = self.history[year]
        for giver, receiver in mapping.items():
            self.require_link(giver, receiver)

    def solve(self) -> list[GiversReceivers]:
        """
        Generate solutions to the gift exchange problem.
        """
        p = constraint.Problem()
        for giver, receivers in self.constraints.items():
            p.addVariable(giver, list(receivers))
        p.addConstraint(constraint.AllDifferentConstraint(), EVERYONE)
        start = time.monotonic()
        solutions = p.getSolutions()
        end = time.monotonic()
        elapsed = end - start
        print(f"{len(solutions)} solutions in {elapsed:.2e} seconds")
        return solutions


def choose_solution(solutions: list[GiversReceivers], *, seed) -> GiversReceivers:
    random.seed(seed)
    random.shuffle(solutions)
    return solutions[0]


def print_giver_receiver_pairs(solution: GiversReceivers):
    givers_receivers = sorted(solution.items(), key=lambda pair: str(pair[0]))
    for giver, receiver in givers_receivers:
        print(f"{giver.name.capitalize():>10s} -> {receiver.name.capitalize()}")


def solve_2023():
    exchange = GiftExchange()
    exchange.require_link(Person.JOSIE, Person.GERALDINE)
    exchange.require_link(Person.GERALDINE, Person.JOSIE)
    exchange.forbid_history(2020)
    exchange.forbid_history(2021)
    exchange.forbid_history(2022)
    solutions = exchange.solve()
    return choose_solution(solutions, seed='2023')


def solve_2024():
    exchange = GiftExchange()
    for year in range(2016, 2024):
        exchange.forbid_history(year)
    solutions = exchange.solve()
    return choose_solution(solutions, seed='2024')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("year", type=int, help="Year to generate/print solutions for.")
    args = parser.parse_args()

    year = args.year

    solution_funcs = {
        2023: solve_2023,
        2024: solve_2024,
    }

    get_history_func = partial(GiftExchange.get_history, year)
    solution_func = solution_funcs.get(year, get_history_func)
    solution = solution_func()
    print_giver_receiver_pairs(solution)



if __name__ == '__main__':
    main()
